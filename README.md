**Présentation courte du projet**
_Vector_ est un dispositif permettant de rendre compte des échanges ayant lieu entre l'Hôtel Pasteur et les hôtes de passage, de manière ludique. 

**Note technique**

**Quel dispositif technique utilise le projet ?**
Ce projet utilise une stick arcade relié à un raspi et à un écran.
Les touches correspondent aux contributions des hôtes, les directions du joystick aux contributions de l'Hôtel Pasteur.
De manière immédiate les inforamtions rentrées apparaissent de manière graphique sous la formes de segements de couleurs.
À plus long terme ces données peuvent être visualisées avec des graphiques.

**Quel matériel nécessite le projet ?**

L'usager n'a pas besoin de dispositif particulier.
Pour l'objet en lui-même il faut:
- un joystick
- 7 boutons arcades (5 pour les couleurs et deux pour le zoom et dezoom)
- une raspi
- un écran

**A quelle fréquence le dispositif envoie des données**
Les données enregistrées sur le dispositif sont transmis une fois par mois au site.
Nous ne savons pas où sur celui-ci le dessin produit par les hôtes est montré.
Nous ne savons pas où la partie "graphique" est présentée.
Il faut sans doute une intervention humaine pour téléverser ces informations.

**Schéma électronique du dispositif**

**Notice d'installation**
Pour fonctionner le dispositif nécessite :

Un logiciel de dessin conçu pour
